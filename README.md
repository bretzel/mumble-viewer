# Mumble viewer

## Requirements
- A webserver
- A PHP server
- A working [Murmur-rest API](https://github.com/alfg/murmur-rest)

## Installation

- Copy the files in your website directory
- Edit index.php to adapt it to your configuration
	- You may need to change this line with your Murmur-rest API URL:
	`$API = json_decode(file_get_contents("http://localhost:8181/servers/1/"), true);`

## Systemd service example for Murmur-rest API production server

```
[Unit]
Description=Mumble API Server
After=network.target

[Service]
User=site
WorkingDirectory=<path-to>/murmur-rest/
ExecStart=gunicorn -b 127.0.0.1:8181 wsgi:app

[Install]
WantedBy=multi-user.target
```